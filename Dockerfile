FROM node:alpine

COPY src /nodejs/src

COPY package.json /nodejs/package.json
COPY tsoa.json /nodejs/tsoa.json
COPY tsconfig.json /nodejs/tsconfig.json
COPY tslint.json /nodejs/tslint.json

WORKDIR /nodejs

RUN npm install
RUN npm audit fix

RUN npm run build

EXPOSE 3000:3000

CMD npm start

