### GROCERY ALERT API

### Prerequisites

- npm 6.9.0 or higher
- Node.js 10.15.3 or higher
- MongoDB database `mongod --dbpath /tmp/api`

`curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -`
`sudo apt-get install -y nodejs`

### Set up the Development Environment

Copy the `.env.example` file and rename it to `.env`.\

In this file you have to configure the environment variables.


### Install

- Install all dependencies with `npm install`

### Generate swagger docs

- Run tsoa using `npm run generate:docs`
- Run development mode using `npm run dev`
- The swagger docs ui will be displayed to you as `http://localhost:3000/docs`

### Running in dev mode

- Run `npm run dev` to start nodemon with ts-node, to serve the app.
- The server address will be displayed to you as `http://localhost:3000`

### Building the project and run it

- Run `npm run build` to generated all JavaScript files from the TypeScript sources.
- To start the builded app located in `dist` use `npm start`.

### Deployment via Docker
Requirements:

- Docker must be installed and locatable in the `PATH` in your workstation

Instructions:
    TODO

## ❯ Environment file

NODE_ENV - development | test | production
