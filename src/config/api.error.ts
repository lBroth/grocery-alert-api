/**
 * Class representing an API error.
 * @extends Error
 */

export class APIError extends Error {
  public message: any;
  public name: string;
  public status: number;
  public stack: string;
  public isPublic: boolean;
  public timestamp: string;

  /**
   * Creates an API error.
   * @param {string} message - Error message.
   * @param {number} status - HTTP status code of error.
   * @param {string} stack - Error stack.
   * @param {boolean} isPublic - Whether the message should be visible to user or not.
   */
  constructor(message: any, status: number = 500, stack: string | object, isPublic: boolean = false) {
    super();
    this.name = this.constructor.name;
    this.message = message;
    this.status = status;
    this.stack = (typeof stack === "string" ? stack : JSON.stringify(stack)) as string;
    this.isPublic = isPublic;
    this.timestamp = new Date().toISOString();
  }
}
