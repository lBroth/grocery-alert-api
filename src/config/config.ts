import * as dotenv from "dotenv";

dotenv.config({path: ".env"});

function stringToArray(value: string, separator = ","): string[] {
  return (value || "").split(separator).map((s) => s.trim()).filter((s) => s !== "");
}

export interface IConfig {
  ENV: string;
  PORT: number;
  SWAGGER_ENABLED: boolean;
  DATA_DIR: string;
  CORS_EXTRA_HOSTS: string[];
  SESSION_SECRET: string;
  CRON_CHECK_CORTILIA: string;
  CRON_CHECK_CORTILIA_ENABLED: boolean;
  CRON_CHECK_CORTILIA_RUN_AT_STARTUP: boolean;

  DB_ORM_ENTITIES: string[];
  DB_HOST: string;
  DB_NAME: string;
  DB_PORT: number;
}

const config: IConfig = {
  ENV: process.env.ENV || "development",
  PORT: process.env.PORT ? +process.env.PORT : 3000,
  SWAGGER_ENABLED: process.env.SWAGGER_ENABLED ? process.env.SWAGGER_ENABLED === "true" : false,
  DATA_DIR: process.env.DATA_DIR || "/tmp/api",
  CORS_EXTRA_HOSTS: stringToArray(process.env.CORS_EXTRA_HOSTS) || [],
  SESSION_SECRET: process.env.SESSION_SECRET || "skupwlstvdbs453823bdsls7e42bwk",

  CRON_CHECK_CORTILIA: process.env.CRON_CHECK_CORTILIA,
  CRON_CHECK_CORTILIA_ENABLED: process.env.CRON_CHECK_CORTILIA_ENABLED === "true",
  CRON_CHECK_CORTILIA_RUN_AT_STARTUP: process.env.CRON_CHECK_CORTILIA_RUN_AT_STARTUP === "true",

  DB_ORM_ENTITIES: stringToArray(process.env.DB_ORM_ENTITIES),
  DB_HOST: process.env.DB_HOST,
  DB_NAME: process.env.DB_NAME,
  DB_PORT: +process.env.DB_PORT,

};

export {config};
