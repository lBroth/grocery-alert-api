import {dataDir} from "../util/fsdata";

export interface IConstant {
  BODY_PARSER_LIMIT: string;
  JOI_ABORT_EARLY: boolean;
  LOG_PATH: string;
  JWT_ACCESS_EXPIRES_IN: string;
  JWT_SECRET_EXPIRES_IN: string;
  JWT_ALGORITHM_ACCESS: string;
  JWT_ALGORITHM_SECRET: string;

}

export const constant: IConstant = {
  BODY_PARSER_LIMIT: "50kb",
  JOI_ABORT_EARLY: false,
  LOG_PATH: dataDir("log"),
  JWT_ACCESS_EXPIRES_IN: "30d",
  JWT_SECRET_EXPIRES_IN: "365d",
  JWT_ALGORITHM_ACCESS: "HS256",
  JWT_ALGORITHM_SECRET: "HS512",
};
