import cors from "cors";
import {APIError} from "./api.error";
import {config} from "./config";

const options = {
  origin: (origin: string, callback: any) => {
    const whiteList = [
      "localhost",
      "127.0.0.1",
      ...config.CORS_EXTRA_HOSTS
    ]; // TODO - set up hostnames in env
    // We are doing string matching here.
    // For advanced use-case, use regex
    if (!origin) {
      callback(null, true);
    } else if ( whiteList.findIndex((aWhiteListedOrigin) => origin.includes(aWhiteListedOrigin)) !== -1 ) {
      callback(null, true);
    } else {
      const error = {
        isPublic: true,
        message: `'${origin}' is not allowed to access the specified route/resource`,
        stack: "",
        status: 403,
      };
      callback(new APIError(error.message, error.status, error.stack, error.isPublic), false);
    }
  }
};

export default cors(options);
