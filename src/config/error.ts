import {NextFunction, Request, Response} from "express";
import {config} from "./config";
import {logger} from "./logger";
import {APIError} from "./api.error";

// error handler, stack just on development
interface IExpressError {
  isPublic: boolean;
  stack: any;
  message: any;
  status: number;
}

/**
 * Error handler. Send stacktrace only during development
 */
const handler = (err: IExpressError, req: Request, res: Response, next: NextFunction) => {
  const response = {
    IP: req.ip,
    URL: req.originalUrl,
    code: err.status || 500,
    message: err.isPublic ? err.message : err.status,
    stack: err.stack,
    timestamp: new Date().toISOString(),
  };

  if (config.ENV !== "development" && config.ENV !== "test") {
    logger.error(JSON.stringify(response));
    delete response.stack;
  }
  res.status(response.code);
  res.json(response);

};

/**
 * Catch 404 and forward to error handler
 */
const notFound = (req: Request, res: Response, next: NextFunction) => {
  const err = new APIError("Not found", 404, "", true);
  return handler(err, req, res, next);
};

const error = {
  handler,
  notFound
};

export {error};
