import bodyParser from "body-parser";
import compress from "compression";
import cookieParser from "cookie-parser";
import expHbs from "express-handlebars";
import express from "express";
import expressWinston from "express-winston";
import helmet from "helmet";
import morgan from "morgan";
import "reflect-metadata";
import {router as routes} from "./router";
import {error} from "./error";
import cors from "./cors";
import {logger, logger as winstonInstance} from "./logger";
import packageJson from "../../package.json";
import {config} from "./config";
import {constant} from "./constant";
import {CronService} from "../cron/cron.service";
import {registerDBConnections} from "./typeorm";

export async function createBackendApp(): Promise<express.Application> {
  try {
    const app: express.Application = express();

    await registerDBConnections();
    app.enable("trust proxy");

    // setup winston stream for morgan logger
    if (config.ENV === "development") {
      app.use(morgan("dev"));
    } else if (config.ENV === "production") {
      class WinstonStream {
        public write(text: string) {
          winstonInstance.info(text);
        }
      }

      const winstonStream = new WinstonStream();
      app.use(morgan("combined", {stream: winstonStream}));
    }

    // parse body params
    app.use(bodyParser.urlencoded({extended: false, limit: constant.BODY_PARSER_LIMIT}));
    app.use((req, res, next) => {
      if (req.originalUrl.startsWith("/payment/stripe/hook")) {
        logger.debug(`Detected /payment/stripe/hook call, using bodyParser.raw`);
        bodyParser.raw({type: "*/*"})(req, res, next);
      } else {
        bodyParser.json({limit: constant.BODY_PARSER_LIMIT})(req, res, next);
      }
    });

    app.use(cookieParser());

    app.use(compress());

    // secure apps, HTTP headers
    app.use(helmet());

    // verify api status
    app.get("/health-check", (req, res) => {
      res.status(200).send(packageJson.version);
    });

    // enable CORS
    app.use(cors);

    // enable Handlebars as view engine
    app.engine(".hbs", expHbs({extname: ".hbs"}));
    app.set("view engine", ".hbs");
    app.use("/static", express.static("static"));

    // enable detailed API logging
    expressWinston.requestWhitelist.push("body");
    expressWinston.responseWhitelist.push("body");
    app.use(expressWinston.logger({
      meta: true,
      msg: "HTTP {{req.method}} {{req.url}} {{res.statusCode}} {{res.responseTime}} ms.",
      bodyBlacklist: ["password", "passwordSalt", "passwordHash", "Authorization", "authorization"],
      winstonInstance,
    }));

    app.use("/", routes);

    // catch 404 and forward to error handler
    app.use(error.notFound);

    // error handler, send stacktrace only during development
    app.use(error.handler);

    // setup cron jobs
    CronService.setupJobs();

    return app;

  } catch (e) {
    logger.error("express config error", e);
  }
}
