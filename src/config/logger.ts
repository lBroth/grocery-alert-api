import {createLogger, format, Logger, transports} from "winston";
import DailyRotateFile from "winston-daily-rotate-file";
import {config} from "./config";
import {constant} from "./constant";

const options = {
  accessLog: {
    filename: `${constant.LOG_PATH}/access-%DATE%.log`,
    datePattern: "YYYY-MM-DD",
    maxFiles: "365d",
    level: "debug",
    handleExceptions: true
  },
  console: {
    colorize: true,
    handleExceptions: true,
    json: false,
    level: "debug"
  },
  errorLog: {
    filename: `${constant.LOG_PATH}/error-%DATE%.log`,
    datePattern: "YYYY-MM-DD",
    maxFiles: "365d",
    level: "error",
    handleExceptions: true
  },
  warningLog: {
    filename: `${constant.LOG_PATH}/warning-%DATE%.log`,
    datePattern: "YYYY-MM-DD",
    maxFiles: "365d",
    level: "warning",
    handleExceptions: true
  },
  exception: {
    filename: `${constant.LOG_PATH}/exceptions-%DATE%.log`,
    datePattern: "YYYY-MM-DD",
    maxFiles: "365d"
  }
};

const logger: Logger = createLogger({
  exceptionHandlers: [
    new DailyRotateFile(options.exception),
    new transports.Console()
  ],
  exitOnError: false, // do not exit on handled exceptions
  format: format.combine(
    format.timestamp(),
    format.json(),
    format.splat(),
    format.simple()
  ),
  transports: []
});
if (config.ENV === "test") {
  // no console
  // logger.add(new transports.Console(options.console));
} else if (config.ENV === "development") {
  logger.add(new transports.Console(options.console));
} else {
  logger.add(new transports.Console(options.console));
  logger.add(new DailyRotateFile(options.accessLog));
  logger.add(new DailyRotateFile(options.warningLog));
  logger.add(new DailyRotateFile(options.errorLog));
}

export {logger};
