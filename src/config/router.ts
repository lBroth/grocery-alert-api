import express, {NextFunction, Request, Response} from "express";
import swaggerUi from "swagger-ui-express";
import * as packageJSON from "../../package.json"
import {config} from "./config";
import {logger} from "./logger";
import swaggerDocument = require("../../swagger/swagger.json");
import UserController from "../user/user.controller";
import {Cat} from "../user/cat.type";
import {APIError} from "./api.error";
import User from "../user/user.model";


const router = express.Router();


/** MISC */
router.route("/").get((req: Request, res: Response) => {
  return res.status(200).send("");
});

router.route("/version").get((req: Request, res: Response) => {
  return res.status(200).send({version: packageJSON.version});
});

// SWAGGER DOCS
if (config.SWAGGER_ENABLED) {
  logger.warn("Swagger docs enabled");
  router.use("/docs", swaggerUi.serve);
  router.get("/docs", swaggerUi.setup(swaggerDocument));
}

router.route("/cats").get(
  (req: Request, res: Response, next: NextFunction) => {
    UserController.getCats()
      .then((result: Cat[]) => res.status(200).json(result))
      .catch((e: APIError) => next(e));
  }
);

router.route("/saveUser").get(
  (req: Request, res: Response, next: NextFunction) => {
    UserController.addUser()
      .then((result: User) => res.status(201).json(result))
      .catch((e: APIError) => next(e));
  }
);

router.route("/getUser/:deviceUUID").get(
  (req: Request, res: Response, next: NextFunction) => {
    UserController.getUser(req.params.deviceUUID)
      .then((result: User) => res.status(200).json(result))
      .catch((e: APIError) => next(e));
  }
);
export {router};
