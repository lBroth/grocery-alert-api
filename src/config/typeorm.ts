import {ConnectionOptions, createConnections, getConnectionManager} from "typeorm";
import {config} from "./config";

// TODO remove "database" when bug https://github.com/typeorm/typeorm/issues/2096 is solved
export async function registerDBConnections() {
  let connections = [{
    name: "default",
    type: "mongodb",
    host: config.DB_HOST,
    port: config.DB_PORT,
    database: config.DB_NAME,
    logging: true,
    timezone: "Z",
    entities: config.DB_ORM_ENTITIES,
    useUnifiedTopology: true
  } as ConnectionOptions];
  console.log(connections);

  if (config.ENV === "test") {
    connections = [{
      name: "default",
      type: "mongodb",
      host: config.DB_HOST,
      port: config.DB_PORT,
      database: config.DB_NAME,
      logging: false,
      timezone: "Z",
      entities: config.DB_ORM_ENTITIES,
      useUnifiedTopology: true
    } as ConnectionOptions];
  }
  await createConnections(connections);
}

export function getDBConnection() {
  const connectionName = /*config.ENV === "test" ? "test" : */"default";
  return getConnectionManager().get(connectionName);
}
