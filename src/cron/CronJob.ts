export abstract class CronJob {

  public readonly abstract name: string;

  public readonly abstract cronTab: string;

  public readonly enabled: boolean = true;

  public readonly runAtStartup: boolean = true;

  public abstract async run(): Promise<boolean>;

}
