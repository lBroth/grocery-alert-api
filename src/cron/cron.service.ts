import cron from "cron";
import {logger} from "../config/logger";
import {CronJob} from "./CronJob";
import {CheckCortilia} from "./impl/CheckCortilia";
import {CheckCarrefour} from "./impl/CheckCarrefour";


export class CronService {

  public static setupJobs() {
    this.jobs
      .filter((jobDef: CronJob) => jobDef.enabled)
      .forEach(async (jobDef: CronJob) => {
        const job = new cron.CronJob(jobDef.cronTab, async () => {
          logger.info(`Starting cron job ${jobDef.name}`);
          try {
            const success = await jobDef.run();
            logger.info(`Cron job ${jobDef.name} completed: ${success ? "SUCCESS" : "FAILURE"}`);
          } catch (e) {
            logger.error(`Cron job ${jobDef.name} failed %j`, e);
          }
        });
        job.start();
        if (jobDef.runAtStartup) {
          await jobDef.run();
        }
      });
  }

  private static jobs: CronJob[] = [
    new CheckCortilia(),
    // new CheckCarrefour(),
  ];

}
