import {config} from "../../config/config";
import {logger} from "../../config/logger";
import {CronJob} from "../CronJob";

export class CheckCarrefour extends CronJob {

  public readonly cronTab: string = config.CRON_CHECK_CORTILIA;
  public readonly enabled: boolean = config.CRON_CHECK_CORTILIA_ENABLED;
  public readonly runAtStartup: boolean = config.CRON_CHECK_CORTILIA_RUN_AT_STARTUP;
  public readonly name: string = "CheckCarrefour";

  public async run(): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
      const Nightmare = require("nightmare");
      const nightmare = Nightmare({
        show: true,
        switches: {"ignore-certificate-errors": true},
        width: 1024,
        height: 600,
        webPreferences: { devTools: 'right' },
        waitTimeout: 100000,
      });
      nightmare
        .goto("https://www.carrefour.it")
        .wait(1000)
        .screenshot(`/Users/simone/Desktop/${this.name}.jpeg`)
        .evaluate(() => document.querySelector('#lbHeaderH2').textContent)
        .end().then((text: string) => {
          logger.info(`${this.name}: not available`);
          return reject(text)
        }).catch((e: any) => {
          if (e.code === -3) {
            logger.info(`${this.name}: not available`);
            return reject(e);
          }
          logger.info(`${this.name}: available`);
          return resolve(true)
        });
      });
  }

}
