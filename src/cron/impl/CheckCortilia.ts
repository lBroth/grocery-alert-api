import {config} from "../../config/config";
import {logger} from "../../config/logger";
import {CronJob} from "../CronJob";

export class CheckCortilia extends CronJob {

  public readonly cronTab: string = config.CRON_CHECK_CORTILIA;
  public readonly enabled: boolean = config.CRON_CHECK_CORTILIA_ENABLED;
  public readonly runAtStartup: boolean = config.CRON_CHECK_CORTILIA_RUN_AT_STARTUP;
  public readonly name: string = "CheckCortilia";

  public async run(): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
      const Nightmare = require("nightmare");
      const nightmare = Nightmare({
        show: true,
        switches: {"ignore-certificate-errors": true},
        width: 1024,
        height: 600,
        webPreferences: { devTools: 'right' },
        waitTimeout: 100000,
      });
      nightmare
        .goto("https://www.cortilia.it")
        .wait(".signinmultibutton")
        .click(".signinmultibutton")
        .wait("button.btn-email")
        .click("button.btn-email")
        .wait("input#loginEmail")
        .wait("input#loginPassword")
        .insert("input#loginEmail", "simonebellano@yahoo.it")
        .insert("input#loginPassword", "Simone1988!")
        .wait(1000)
        .click("button.btn-email")
        .wait(3000)
        .screenshot(`/Users/simone/Desktop/${this.name}.jpeg`)
        .evaluate(() => document.querySelector('#errorContainer').textContent)
        .end().then((text: string) => {
          logger.info(`${this.name}: not available`);
          return reject(text)
        }).catch((e: any) => {
          logger.info(`${this.name}: available`);
          return resolve(true)
        });
    });

  }

}
