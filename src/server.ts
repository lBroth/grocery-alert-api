import {config} from "./config/config";
import {createBackendApp} from "./config/express";
import {logger} from "./config/logger";
import HTTP from "http";

if (config.ENV !== "test") {
  ( async () => {
    await init();
  })();
}

async function init() {
  return new Promise(async (resolve, reject) => {
    const app = await createBackendApp();
    const server = app.listen(config.PORT, () => {
      logger.info(`server started on port ${config.PORT} (${config.ENV})`);
      const http = new HTTP.Server(app);
      app.emit("appStarted");
      resolve(app);

    });

    process.on("SIGTERM", () => {
      server.close();
    });
  });
}

export default init;
