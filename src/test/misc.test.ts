import chai from "chai";
import request from "supertest";
import * as packageJson from "../../package.json";
import {createBackendApp} from "../config/express";

describe("## Misc ", () => {

  let app: any;
  before( (done) => {
    createBackendApp().then((a) => {
      app = a;
      done();
    });
  });

  describe("# Health check", () => {
    it("should be a version in the package.json", (done: any) => {
      request(app)
        .get("/version")
        .expect(200)
        .then((res: { body: { version: string} }) => {
          chai.expect(res.body.version).to.equal(`${packageJson.version}`);
          done();
        })
        .catch(done);
    });

    it("not found error", (done: any) => {
      request(app)
        .get("/example-not-found-page")
        .expect(404)
        .then(() => {
          done();
        })
        .catch(done);
    });

  });
});
