export type Cat = {
    genus: string;
    name: string;
    isHungry: boolean;
    lastFedDate: Date;
}

