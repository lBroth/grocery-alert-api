import { Cat } from "./cat.type";
import User from "./user.model";
import UserDal from "./user.dal";
import {APIError} from "../config/api.error";

export default class UserController {
  public static async getCats(): Promise<Cat[]> {
   const cats = [
      { genus: "feline", name: "Cosmo", isHungry: true, lastFedDate: new Date() },
      { genus: "feline", name: "Emmy", isHungry: true, lastFedDate: new Date() }
    ];
   console.log({cats});
   return cats;
  }

  public static async addUser(): Promise<User> {
    const user = new User();
    user.deviceUUID = "asfahsfhjasvbhfasjbfah";
    try {
      return await this.getUser(user.deviceUUID);
    } catch (e) {
      return await UserDal.saveUser(user);
    }
  }

  public static async getUser(deviceUUID: string): Promise<User> {
    const user = await UserDal.getUser(deviceUUID);
    if (!user) {
      throw new APIError("Not found", 404, {deviceUUID}, true);
    }
    return user;
  }
}
