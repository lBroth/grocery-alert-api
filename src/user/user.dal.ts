import {getMongoManager} from "typeorm";
import User from "./user.model";

export default class UserDal {
  public static async saveUser(user: User): Promise<User> {
    const manager = getMongoManager();
    return await manager.save(user);
  }
  public static async getUser(deviceUUID: string): Promise<User> {
    const manager = getMongoManager();
    return await manager.findOne(User, { deviceUUID});
  }
}
