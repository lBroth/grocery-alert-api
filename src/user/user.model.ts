import {Entity, ObjectID, ObjectIdColumn, Column, Index} from "typeorm";

@Entity()
export default class User {

  @ObjectIdColumn()
  id: ObjectID;

  @Column()
  @Index({ unique: true })
  deviceUUID: string;

}
