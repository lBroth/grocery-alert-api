import fs from "fs";
import path from "path";
import {config} from "../config/config";

/**
 * Get (and creates if necessary) a subdirectory in DATA_DIR
 * @param name the name of the directory. f.e. "logs" or "/some/subdir"
 */
export function dataDir(name: string) {
  const dirPath = path.join(config.DATA_DIR, name);
  if (!fs.existsSync(dirPath)) {
    fs.mkdirSync(dirPath, {recursive: true});
  }
  return dirPath;
}
